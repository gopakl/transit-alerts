class Agency < ApplicationRecord
    validates :name, presence: true
    validates :city, presence: true
end
