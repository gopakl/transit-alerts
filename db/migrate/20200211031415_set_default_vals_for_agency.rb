class SetDefaultValsForAgency < ActiveRecord::Migration[5.2]
  def change
    change_column_default :agencies, :active, from: nil, to: false
  end
end
